const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

module.exports.addOrder = async (data) => {
  try {
    // Check if an order already exists for the user
    const existingOrder = await Order.findOne({ userId: data.userId });

    if (existingOrder) {
      // Check if the product already exists in the order
      const existingProduct = existingOrder.products.find(product => product === data.productId);

      if (existingProduct) {
        console.log('Product already exists in the order');
        return false;

      } else {
        // If the product doesn't exist in the order, add it by pushing the new product to the existing products array
        const product = await Product.findById(data.productId);
        const subTotal = product.price * data.quantity;

        existingOrder.products.push({ productId: data.productId, quantity: data.quantity, subTotal });
        await existingOrder.save();

        // Recalculate the total amount by adding up all the subtotals for the products in the order using a for loop
        let totalAmount = existingOrder.totalAmount || 0; // Initialize to existing totalAmount or 0
        for (let i = 0; i < existingOrder.products.length; i++) {
          totalAmount += existingOrder.products[i].subTotal;
        }
        existingOrder.totalAmount = totalAmount;
        await existingOrder.save();
      }

    } else {
      // If no order exists, create a new order for the user with the given userId and products array
      const product = await Product.findById(data.productId);
      const subTotal = product.price * data.quantity;

      const products = [{ productId: data.productId, quantity: data.quantity, subTotal }];
      const totalAmount = products.reduce((acc, product) => acc + product.subTotal, 0);

      const order = new Order({
        userId: data.userId,
        products,
        totalAmount
      });
      await order.save();
    }

    return true;
  } catch (err) {
    console.error(err);
    return false;
  }
};


module.exports.getAllOrders = async () => {
  try {
    const orders = await Order.find().populate('userId').populate('products.productId');
    const data = {};

    orders.forEach(order => {
      const userId = order.userId._id.toString();

      if (!data[userId]) {
        data[userId] = { user: order.userId, products: [], totalAmount: 0 };
      }

      const products = order.products.map(product => {
        const productData = {
          productId: product.productId._id,
          name: product.productId.name,
          description: product.productId.description,
          price: product.productId.price,
          quantity: product.quantity,
          subTotal: product.productId.price * product.quantity
        };
        data[userId].totalAmount += productData.subTotal;
        return productData;
      });

      data[userId].products = data[userId].products.concat(products);
    });

    // Map the data object to an array of orders with totalAmount included
    const ordersWithTotalAmount = Object.values(data).map(order => {
      return { ...order, totalAmount: order.totalAmount };
    });

    return ordersWithTotalAmount;
  } catch (err) {
    console.error(err);
    return [];
  }
};

module.exports.getUserOrders = async (userId) => {
  try {
    const orders = await Order.find({ userId }).populate('products.productId');
    const data = { user: null, products: [], totalAmount: 0 };

    orders.forEach(order => {
      if (!data.user) {
        data.user = order.userId;
      }

      const products = order.products.map(product => {
        const productData = {
          productId: product.productId._id,
          name: product.productId.name,
          description: product.productId.description,
          price: product.productId.price,
          quantity: product.quantity,
          subTotal: product.productId.price * product.quantity
        };
        data.totalAmount += productData.subTotal;
        return productData;
      });

      data.products = data.products.concat(products);
    });

    // Return the user's orders with totalAmount included
    return { ...data, totalAmount: data.totalAmount };
  } catch (err) {
    console.error(err);
    return null;
  }
};



module.exports.removeOrder = async (req, res) => {
  const { id } = req.params;

  try {
    // Find the order to be removed
    const order = await Order.findById(id);
    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }

    // Remove the order
    await order.remove();

    res.json({ message: 'Order removed successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};