const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

// ============ CHECKING EMAIL DUPLICATION ============ //
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} 

		else {
			return false;
		}
	}).catch(err => err);
};





// ============ USER REGISTRATION ============ //
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then(user => {

		if(user){
			return true
		} else {

			return false
		}
	}).catch(err => err)
};





// ============ USER AUTHENTICATION/LOGIN ============ //
module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {

    	// USER DOES NOT EXIST
    	// Entered both an email and password but email is not connected to any existing account
 		if (result === null) {
			return false
		}

		// USER EXISTS
		// Entered both an email and password and email is connected to an existing account
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// Correct password
			if (isPasswordCorrect) {
				return {
					status: "Authentication successful.",
					access: auth.createAccessToken(result)
				}
			} 

			// Incorrect password
			else {
				return false
			}
		}
	}).catch(err => err);
};





// ============ RETRIEVAL OF USER'S DETAILS ============ //
// Admin & Customer Access
module.exports.getUserDetails = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		return result;
	}).catch(err => err);
};



// ============ SETTING USER TO ADMIN ============ //
module.exports.setAsAdmin = (reqParams) => {
	let admin = {
		isAdmin: true
	};

	return User.findById(reqParams, admin).then(result => {

		if (!result) {
			return  {
				status: "User does not exist."
			}
		}
		
		if (result.isAdmin) {
			return  {
				status: "User is already an admin."
			}
		}

		else {
			return User.findByIdAndUpdate(reqParams, admin, {new:true}).then(result => { 
				return {
					status: result.firstName + " is now an admin.",
						userData: {
							userId: result.id,
							firstName: result.firstName,
							lastName: result.lastName,
							email: result.email,
							isAdmin: result.isAdmin
						}
				}
			}).catch(err => err);
		}
	})
};





// ============ RETRIEVAL OF ALL USERS ============ //
// Will have to double check if this is neeeded
module.exports.getAllUsers = () => {
	return User.find({}).then(result => result).catch(err => err);
};




