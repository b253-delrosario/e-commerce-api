const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");


// ============ PRODUCT CREATION ============ //
module.exports.createProduct = (reqBody) => {
	return Product.findOne({name:reqBody.name}).then(result => {

		
		// Provided an exisiting active product 
		if (result != null && result.name == reqBody.name && result.isActive) {
			return false
		}

		// Provided an exisiting archived product 
		else if (result != null && result.name == reqBody.name && !result.isActive) {
			return false
		} 

		// Filled out all mandatory fields and provided a new product name.
		else {
			let newProduct = new Product ({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});

			return newProduct.save().then(product => {

				// Successful creation of product
				if (product) {
					return true
				}

				// Error
				else {
					return false
				}
			})
		}
	}).catch(err => err)
};





// ============ RETRIEVAL OF ALL PRODUCTS ============ //
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result).catch(err => err);
};





// ============ RETRIEVAL OF ALL ACTIVE PRODUCTS ============ //
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive:true}).then(result => result).catch(err => err);
};






// ============ RETRIEVAL OF SPECIFIC PRODUCTS ============ //
module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {

		// Archived Product - Admin Access
		if (!result.isActive) {
			return result;
		}

		// Active Product
		else {
			return result;
		}
	}).catch(err => err)
};







// ============ UPDATE PRODUCT INFORMATION ============ //
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	// "The product is already archived and cannot be further modified. Please choose an active product to edit."

	return Product.findByIdAndUpdate(reqParams, updatedProduct, {new:true}).then(updatedProduct => {
		if (updatedProduct) {
			return true

		} else {
			return false
		}
	}).catch(err => err);
};






// ============ ARCHIVAL OF PRODUCTS ============ //
module.exports.archiveProduct = (reqParams) => {
	let archivedProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams, archivedProduct).then(product => {
		if (!product.isActive) {
			return false
		}

		else {
			return true
		}
	}).catch(err => err)
};





// ============ ACTIVATION OF PRODUCTS ============ //
module.exports.activateProduct = (reqParams) => {
	let activatedProduct = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams, activatedProduct).then(product => {
		if (product.isActive) {
			return false
		} 

		else {
			return true
		}
	}).catch(err => err)
};





// ============ RETRIEVAL OF ALL ARCHIVED PRODUCTS ============ //
// Will have to double check if needed
module.exports.getAllArchivedProducts = () => {
	return Product.find({isActive:false}).then(result => result).catch(err => err);
};







