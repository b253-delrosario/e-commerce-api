const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;

mongoose.connect("mongodb+srv://admin:admin123@batch253-delrosario.zfa9ghi.mongodb.net/ecommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

db.once("open", () => console.log("You are now connected to MongoDB Atlas!"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);


if(require.main === module) {
	app.listen(port, () => {
		console.log(`API is now online on port ${port}.`)
	})
};

module.exports = app;