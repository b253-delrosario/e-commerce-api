const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	products: [
		{
			productId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product',
				required: true
			},
			quantity: {
				type: Number,
				required: [true, "Product quantity is required"]
			},
			subTotal: {
				type: Number
			}
		}
	],
	totalAmount: {
		type: Number
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	status: {
		type: String,
		default: "Order placed."
	}
});

module.exports = mongoose.model("Order", orderSchema);