const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Please enter your name."]
	},
	description: {
		type: String,
		required: [true, "Please enter the product's description."]
	},
	price: {
		type: Number,
		required: [true, "Please enter the product's price."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}


});

module.exports = mongoose.model("Product", productSchema);

	/*
	stress goals for me: 

	stocks: {
		type: Number,
		required: [true, "Please enter how many stocks you have."]
	}

	*/