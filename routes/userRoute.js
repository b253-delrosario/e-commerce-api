const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for checking email duplication (POST)
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for user registration (POST)
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for user authentication/login (POST)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for retrieving user details (GET, Admin & Customer)
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getUserDetails({userId: userData.id}).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for setting a user to admin (PATCH)
router.patch("/:id/admin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// Admin
	if(userData.isAdmin) {
		userController.setAsAdmin(req.params.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	}

	// Non-admin
	else {
		res.send("You are not authorized to view this page.")
	}
});





// Route for retrieving all users (GET, Admin)
// Will have to double check if this is neeeded
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getAllUsers().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





module.exports = router;