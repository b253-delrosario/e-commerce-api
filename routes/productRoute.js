const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product (POST - admin only)
router.post("/create", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for retrieving all products (GET - admin only)
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.getAllProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		return false
	}
});
		





// Route for retrieving all ACTIVE products (GET)
router.get("/active", (req, res) => {
	productController.getAllActiveProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for retrieving a SINGLE Product (GET)
router.get("/:id", (req, res) => {
	productController.getSpecificProduct(req.params.id, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});




// Route for updating product information (PUT - admin only)
router.put("/:id/update", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.updateProduct(req.params.id, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send("You are not authorized to update a product.")
	}
});





// Route for archiving product (PATCH - admin only)
router.patch("/:id/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.archiveProduct(req.params.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send("You are not authorized to archive a product.")
	}
});





// Route for activating product (PATCH - admin only)
router.patch("/:id/activate", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.activateProduct(req.params.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send("You are not authorized to activate a product.")
	}
});





// Route for retrieving all ARCHIVED products (GET)
router.get("/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.getAllArchivedProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		return false
	}
});

module.exports = router;






