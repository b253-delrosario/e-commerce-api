const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const productController = require("../controllers/productController");
const userController = require("../controllers/userController");
const auth = require("../auth");



// Route for adding product to cart
router.post("/add-to-cart", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId : userData.id,
        isAdmin : userData.isAdmin,
        productId : req.body.productId,
        quantity : req.body.quantity,
    }

    if(!data.isAdmin){
        orderController.addOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }
});

// Route for retrieving all orders
router.get("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    let data = {
            userId: userData.id,
            isAdmin: userData.isAdmin
        }

    if (data.isAdmin) {
        orderController.getAllOrders().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send("You are not authorized to do such action.");
    }
});

router.get("/user-orders", auth.verify, async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const userOrders = await orderController.getUserOrders(userData.id);

    if (userOrders) {
      res.send(userOrders);
    } else {
      res.status(404).send("No orders found for this user.");
    }
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal server error.");
  }
});

// GET request to clear all orders for the user
router.delete('/checkout', auth.verify, async (req, res) => {
  try {
    // Get the current user's ID
    const userData = auth.decode(req.headers.authorization);
    const userId = req.user.id;

    // Remove all orders for the user from the database
    await Order.deleteMany({ user: userId });

    // Send a success response
    res.status(200).json({ message: 'Orders cleared successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server Error' });
  }
});


module.exports = router;
